
// ToolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Tool.h"
#include "ToolDlg.h"
#include "afxdialogex.h"

#include "Python.h"
#include <regex>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CToolDlg 对话框

CToolDlg *g_dlg = NULL;

CToolDlg::CToolDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_TOOL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CToolDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CToolDlg::OnBnClickedButton1)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CToolDlg 消息处理程序

BOOL CToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	
	g_dlg = this;
	SetTimer(9527, 200, 0);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CToolDlg::WriteNumsToFile()
{
	char projectName[MAX_PATH] = { 0 };
	GetPrivateProfileStringA("config", "project", NULL, projectName, sizeof(projectName), ".\\config.ini");
	strcat(projectName, "-");
	strcat(projectName, "[0-9]+");

	char szStr[1024 * 10] = { 0 };
	GetDlgItemTextA(m_hWnd, IDC_EDIT2, szStr, 1024 * 10);

	std::string str = szStr;
	std::smatch results;
	std::string::const_iterator it = str.begin();
	std::string::const_iterator end = str.end();
	std::vector<std::string> v;
	const std::regex reg(projectName);
	while (std::regex_search(it, end, results, reg)) {
		v.push_back(results[0]);
		it = results[0].second;
	}

	if (v.size() == 0) {
		AfxMessageBox(L"单号内容好像不太对哦~");
		return false;
	}

	DeleteFile(L"input.txt");
	FILE *f = fopen("input.txt", "w+");
	for (auto it = v.begin(); it != v.end(); it++) {
		fwrite(it->c_str(), it->length(), 1, f);
		fwrite("\r\n", 2, 1, f);
	}
	fclose(f);
	return true;
}

UINT ThreadFunc(LPVOID lpParam)
{
	if (g_dlg->WriteNumsToFile()) {
		char pythonHome[MAX_PATH] = { 0 };
		GetPrivateProfileStringA("config", "pythonpath", NULL, pythonHome, sizeof(pythonHome), ".\\config.ini");

		Py_SetPythonHome(pythonHome);
		Py_Initialize();
		assert(Py_IsInitialized());

		auto pName = PyString_FromString("main");
		auto pModule = PyImport_Import(pName);
		auto pDict = PyModule_GetDict(pModule);
		auto pFunc = PyDict_GetItemString(pDict, "main");
		auto pArgs = PyTuple_New(0);
		PyObject_CallObject(pFunc, pArgs);
		Py_DECREF(pModule);
		Py_Finalize();
	}

	g_dlg->GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
	return 0;
}

void CToolDlg::OnBnClickedButton1()
{
	GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
	AfxBeginThread(ThreadFunc, 0);
}


void CToolDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 9527) {
		FILE* fp = NULL;
		errno_t er = fopen_s(&fp, ".t_issue.log", "r");
		if (er == 0) {
			char line[256] = { 0 };
			fgets(line, 256, fp);
			fclose(fp);

			std::string str = "Status:";
			str += line;
			SetDlgItemTextA(m_hWnd, IDC_STATIC_TIP, str.c_str());
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}

HBRUSH CToolDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	//if (pWnd->GetDlgCtrlID() == IDC_STATIC_TIP)
	//{
	//	pDC->SetTextColor(RGB(0, 220, 196));
	//}

	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}

BOOL CToolDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN)
	{
		BOOL bCtrl = ::GetKeyState(VK_CONTROL) & 0x8000;

		switch (pMsg->wParam)
		{
		case 'A':
			if (bCtrl) {
				CEdit *box = (CEdit*)GetDlgItem(IDC_EDIT2);
				box->SetSel(0, -1);
			}
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
